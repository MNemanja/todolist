$(document).ready(function() {
    // initialize calendar (kalendar koristeći JQuery)
    $('#deadline').datepicker({
        dateFormat: "yy-mm-dd",
        minDate: '+1d'
    });

    $('#actionFilter').change(function() {

        let filter = {}
        if ($(this).val() !== '') {
            filter = {
                action: $(this).val()
            }
        }

        // Clear data from html DOM (čišćenje podataka)
        const ul = document.querySelector('.cont_princ_lists > ul');
        ul.innerHTML = '';

        // Load already saved data (učitavanje podataka sa servera)
        $.ajax({
            url: 'https://my-json-server.typicode.com/nebojsazr/todo_service/todos',
            method: 'GET',
            data: filter,
            dataType: 'json'
        }).done(function(data) {
            if (data.length > 0) {
                for (i = 0; i < data.length; i++) {
                    // console.log('element ' + i, data[i]);
                    data[i].date = new Date(data[i].date);
                    createListItem(data[i]);
                }
            }
        }).fail();
    });


    let test;
    // Load already saved data
    $.ajax({
        url: 'https://my-json-server.typicode.com/nebojsazr/todo_service/todos',
        method: 'GET',
        dataType: 'json'
    }).done(function(data) {
        if (data.length > 0) {
            for (i = 0; i < data.length; i++) {
                // console.log('element ' + i, data[i]);
                data[i].date = new Date(data[i].date);
                createListItem(data[i]);
            }
        }

        test = 'Ovo je neka vrednost'

    }).fail();

    setTimeout(function() {
        console.log('TEST: ' + test);
    }, 1000);

});

function add_new() {
    // selektovati element  u kojem se nalazi forma
    const forma = document.querySelector('.cont_crear_new');

    // Dodavanje ili oduzimanje css klase koja cini da forma postane vidljiva
    forma.classList.toggle('cont_crear_new_active');
}


function add_to_list() {

    const deadline = new Date(document.getElementById('deadline').value);
    // Podatke iz elemenata forme prebacujemo u promenljive da bismo ih kasnije iskoristili
    const elements = {
        action: document.getElementById('action_select'),
        title: document.querySelector('.input_title_desc'),
        date: deadline,
        desc: document.querySelector('.input_description')
    }

    if (!isValid(elements)) {
        return false;
    }

    createListItem({
        action: elements.action.value,
        title: elements.title.value,
        date: elements.date,
        desc: elements.desc.value
    });

}

function createListItem(itemData) {
    // Kreiramo node za postojeci html element u koji cemo dodavati nove elemente (planove za uraditi)
    const ul = document.querySelector('.cont_princ_lists > ul');

    // Ovde cuvamo koliko child nodova (li elemenata trenutno ima u listi)
    const childNum = ul.children.length;

    // Krecemo sa kreiranjem novog child noda koji se dodaje prilikom jednoj izvrsavanja ove funkcije

    // Kreiramo osnovni child node element
    const li = document.createElement('li');
    // i dodajemo mu css klasu
    li.classList.add('list_' + itemData.action.toLowerCase());

    // dodajemo mu jos jednu klasu koja sadrzi redni broj elementa
    // Pomocu ove klase mozemo jedinstveno identifikovati element
    const itemClass = 'li_num_0_' + (childNum + 1);
    li.classList.add(itemClass);

    // Kreiramo header koji će sadržati preostalo vreme i dugme za brisanje zadatka
    const itemHeader = document.createElement('div');
    itemHeader.classList.add('item-header');
    li.appendChild(itemHeader);

    // Create leftTimeContainer (ovo prosledjujemo headeru,vreme preostalo za izvršenje zadatka)
    const leftTimeContainer = document.createElement('span');
    leftTimeContainer.classList.add('left-time-container');
    let leftTimeContainerText = 'Time left: ';
    let leftTime = leftUntil(itemData.date);
    leftTimeContainerText += `${leftTime.months} meseci 
                                ${leftTime.days} dana
                                ${leftTime.hours} sati
                                ${leftTime.minutes} minuta
                                ${leftTime.seconds} sekundi`;

    leftTimeContainer.textContent = leftTimeContainerText;
    itemHeader.appendChild(leftTimeContainer);
    // dodavanje stila headeru (preko funkcije colorize),parametar preostali dani da se izvrši radnja iz liste;
    itemHeader.style.border = colorize(leftTime.days);

    // Kreiramo div koji predstavlja prvu kolonu i sadrzi podatak 'action' iz forme;
    const div1 = document.createElement('div');
    div1.className = 'col_md_1_list'; // Dodajemo mu njegovu klasu
    div1.innerHTML = `<p>${itemData.action}</p>`; // i dodajemo mu njegov sadržaj

    // Kreiramo div koji predstavlja drugu kolonu i sadrzi podatke 'title' i 'description' iz forme;
    const div2 = document.createElement('div');
    div2.className = 'col_md_2_list'; // dodajemo mu njegovu klasu

    // Kreiramo pod element title kao node da bismo mogli da operisemo sa njim
    const div2Title = document.createElement('h4');
    div2Title.textContent = itemData.title;

    // Kreiramo pod element za description kao node da bismo mogli da operisemo sa njim
    const div2Desc = document.createElement('p');
    div2Desc.classList.add(`desc${childNum+1}`); // Dodajemo mu jedinstvenu klasu koristeci redni broj
    div2Desc.textContent = itemData.desc; // i dodajemo mu text content

    // Dodajemo novokreirane podelemente u drugu kolonu
    div2.appendChild(div2Title);
    div2.appendChild(div2Desc);

    // Kreiramo event listener za click event nad elementom koji predstavlja title
    div2Title.addEventListener('click', function() {
        let desc = document.getElementsByClassName('desc' + (childNum + 1))[0]; // Selektujemo description element sa odredjenim rednim brojem
        desc.classList.toggle('hidden'); // Sakrivamo descritopn element koristeci css classu hidden
    });

    // Kreiramo div koji predstavlja trecu kolonu i sadrzi podatak 'date' iz forme;
    const div3 = document.createElement('div');
    // idodajemo mu njegovu css klasu
    div3.className = 'col_md_3_list';
    div3.innerHTML = '<div class="cont_text_date"><p>' + itemData.date.toLocaleDateString() + '</p></div>';

    // Kreiramo button elemenent koji ce se koristiti za brisanje tekuceg child elementa od liste 
    const deleteBtn = document.createElement('button');
    deleteBtn.innerHTML = '<i class="fas fa-check-circle"></i>'; // i dodajemo mu text(ikonicu)

    // nad kreiranim buttonnom kreiramo event listener koji brise tekuci li element iz liste
    deleteBtn.addEventListener('click', function() {
        if (confirm('Da li ste sigurni da želite da obrišete?')) {
            ul.removeChild(li); // brisanje stavke liste
        }
    });

    // Dodajemo sve kreirane segmente u glavni li child node
    li.appendChild(div1);
    li.appendChild(div2);
    li.appendChild(div3);
    itemHeader.appendChild(deleteBtn);

    // I konacno dodati kreirani node u document tako da se on pojavljuje u ovom trenutku
    ul.appendChild(li);

    // Nakon sto je novi element kreiran i dodan, imalo bi smisla da se elementi forme resetuju
    // Stoga postavljamo njihove vrednosti na pocetno stanje
    document.getElementById('action_select').value = 'SHOPPING';
    document.querySelector('.input_title_desc').value = '';
    // document.getElementById('date_select').value = 'TODAY';
    document.querySelector('.input_description').value = '';
}

function isValid(elems) {

    elems.title.classList.remove('invalid');
    elems.desc.classList.remove('invalid');


    let result = true;
    if (String(elems.title.value).replace(' ', '') == '') {
        // Uokviriti input
        elems.title.classList.add('invalid');
        result = false;
    }

    if (String(elems.desc.value).trim() == '') {
        // Uokviriti input
        elems.desc.classList.add('invalid');
        result = false;
    }

    const now = new Date();
    if (now.getTime() >= elems.date.getTime()) {
        result = false;
    }



    return result
}
// funkcija colorize mi služi da doda border headeru u boji(u zavisnosti od vremena koje je ostalo da se 
// zadatak izvrsi,tako imamo crvenu,zutu i zelenu boju za border)
function colorize(deadLine) {
    console.log(deadLine);
    if (deadLine >= 2) return '2px double green';
    if (deadLine >= 1) return '2px double yellow';
    if (deadLine >= 0) return '2px double red';
    return;
}